import { Field, ObjectType, ID } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import Table from "./Table";

@Entity()
@ObjectType()
class Restaurant {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  name: string;

  @Field(() => [String])
  @Column("simple-array")
  endorsements: string[];

  @Field(() => [Table])
  @OneToMany(() => Table, (table) => table.restaurant)
  tables: Table[];
}

export default Restaurant;
