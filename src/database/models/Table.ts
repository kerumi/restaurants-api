import { Field, ObjectType, ID } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import Restaurant from "./Restaurant";
import Reservation from "./Reservation";

@Entity()
@ObjectType()
class Table {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  capacity: number;

  @ManyToOne(() => Restaurant, (restaurant) => restaurant.tables)
  @Field(() => Restaurant)
  restaurant: Restaurant;

  @OneToMany(() => Reservation, (reservation) => reservation.table)
  @Field(() => [Reservation])
  reservations: Reservation[];
}

export default Table;
