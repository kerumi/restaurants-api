import { Field, ObjectType, ID } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable } from "typeorm";
import Eater from "./Eater";
import Table from "./Table";

@Entity()
@ObjectType()
class Reservation {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  time: Date;

  @ManyToOne(() => Table, (table) => table.reservations)
  @Field(() => Table)
  table: Table;

  @ManyToMany(() => Eater)
  @JoinTable()
  @Field(() => [Eater])
  diners: Eater[];
}

export default Reservation;
