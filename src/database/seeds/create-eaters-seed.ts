import { Factory, Seeder } from "typeorm-seeding";
import Eater from "../models/Eater";

export default class CreateEaters implements Seeder {
  public async run(factory: Factory): Promise<void> {
    await factory(Eater)().createMany(10);
  }
}
