import { Factory, Seeder } from "typeorm-seeding";
import Restaurant from "../models/Restaurant";
import Table from "../models/Table";

export default class CreateRestaurants implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const restaurants = await factory(Restaurant)().createMany(5);

    for (const restaurant of restaurants) {
      await factory(Table)({ restaurantId: restaurant.id }).createMany(5);
    }
  }
}
