import { MigrationInterface, QueryRunner } from "typeorm";

export class RestaurantsEaters1719700387493 implements MigrationInterface {
    name = 'RestaurantsEaters1719700387493'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "eater" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "dietaryRestrictions" text NOT NULL, CONSTRAINT "PK_bb24d389321be7be7038a4dcb39" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "reservation" ("id" SERIAL NOT NULL, "time" TIMESTAMP NOT NULL, "tableId" integer, CONSTRAINT "PK_48b1f9922368359ab88e8bfa525" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "table" ("id" SERIAL NOT NULL, "capacity" integer NOT NULL, "restaurantId" integer, CONSTRAINT "PK_28914b55c485fc2d7a101b1b2a4" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "restaurant" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "endorsements" text NOT NULL, CONSTRAINT "PK_649e250d8b8165cb406d99aa30f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "reservation_diners_eater" ("reservationId" integer NOT NULL, "eaterId" integer NOT NULL, CONSTRAINT "PK_e5404b194fd8aeea0c50255744a" PRIMARY KEY ("reservationId", "eaterId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_e740a40e7cd1b684a2ceb4672a" ON "reservation_diners_eater" ("reservationId") `);
        await queryRunner.query(`CREATE INDEX "IDX_29f7e220dd6cc524845e96d98d" ON "reservation_diners_eater" ("eaterId") `);
        await queryRunner.query(`ALTER TABLE "reservation" ADD CONSTRAINT "FK_d221f3398a0352970306b3dc676" FOREIGN KEY ("tableId") REFERENCES "table"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "table" ADD CONSTRAINT "FK_bfbf9c025448272dc0453bf8f55" FOREIGN KEY ("restaurantId") REFERENCES "restaurant"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "reservation_diners_eater" ADD CONSTRAINT "FK_e740a40e7cd1b684a2ceb4672a2" FOREIGN KEY ("reservationId") REFERENCES "reservation"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "reservation_diners_eater" ADD CONSTRAINT "FK_29f7e220dd6cc524845e96d98d3" FOREIGN KEY ("eaterId") REFERENCES "eater"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reservation_diners_eater" DROP CONSTRAINT "FK_29f7e220dd6cc524845e96d98d3"`);
        await queryRunner.query(`ALTER TABLE "reservation_diners_eater" DROP CONSTRAINT "FK_e740a40e7cd1b684a2ceb4672a2"`);
        await queryRunner.query(`ALTER TABLE "table" DROP CONSTRAINT "FK_bfbf9c025448272dc0453bf8f55"`);
        await queryRunner.query(`ALTER TABLE "reservation" DROP CONSTRAINT "FK_d221f3398a0352970306b3dc676"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_29f7e220dd6cc524845e96d98d"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e740a40e7cd1b684a2ceb4672a"`);
        await queryRunner.query(`DROP TABLE "reservation_diners_eater"`);
        await queryRunner.query(`DROP TABLE "restaurant"`);
        await queryRunner.query(`DROP TABLE "table"`);
        await queryRunner.query(`DROP TABLE "reservation"`);
        await queryRunner.query(`DROP TABLE "eater"`);
    }

}
