import { faker } from "@faker-js/faker";
import { define } from "typeorm-seeding";
import Table from "../models/Table";

define(Table, (_, context: { restaurantId: number }) => {
  const table = new Table();
  table.capacity = faker.datatype.number({ min: 2, max: 10 });
  table.restaurant = { id: context.restaurantId } as any;
  return table;
});
