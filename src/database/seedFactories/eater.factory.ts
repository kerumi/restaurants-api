import { faker } from "@faker-js/faker";
import { define } from "typeorm-seeding";
import Eater from "../models/Eater";

define(Eater, () => {
  const eater = new Eater();
  eater.name = faker.person.fullName();
  eater.dietaryRestrictions = faker.helpers.arrayElements(
    ["vegan", "vegetarian", "gluten-free", "halal"],
    faker.datatype.number({ min: 0, max: 4 }),
  );
  return eater;
});
