import { faker } from "@faker-js/faker";
import { define } from "typeorm-seeding";
import Restaurant from "../models/Restaurant";

define(Restaurant, () => {
  const restaurant = new Restaurant();
  restaurant.name = faker.company.name();
  restaurant.endorsements = faker.helpers.arrayElements(
    ["vegan", "vegetarian", "gluten-free", "halal"],
    faker.datatype.number({ min: 1, max: 4 }),
  );
  return restaurant;
});
