import { buildSchema } from "type-graphql";
import { graphql } from "graphql";
import RestaurantResolver from "../restaurant.resolver";
import { createReservation, deleteReservation } from "../../services/ReservationService";
import { findRestaurants } from "../../services/RestaurantService";
import Eater from "../../database/models/Eater";
import { dataSource } from "../../config/database";

jest.mock("../../services/ReservationService");
jest.mock("../../services/RestaurantService");
jest.mock("../../config/database");

describe("RestaurantResolver", () => {
  let schema: any;

  beforeAll(async () => {
    schema = await buildSchema({
      resolvers: [RestaurantResolver],
    });
  });

  const eaters = [
    { id: 1, name: "Barney", dietaryRestrictions: ["Vegan"] },
    { id: 2, name: "Spongebob", dietaryRestrictions: ["Gluten-Free"] },
  ];

  const restaurants = [
    {
      id: 1,
      name: "Don Camaron 🦐",
      tables: [
        { id: 1, capacity: 4, reservations: [] },
        { id: 2, capacity: 4, reservations: [] },
      ],
      endorsements: ["Vegan", "Gluten-Free"],
    },
  ];

  (dataSource.getRepository as jest.Mock).mockImplementation((entity) => {
    if (entity === Eater) {
      return {
        find: jest.fn().mockResolvedValue(eaters),
      };
    }
    return null;
  });

  beforeEach(() => {
    jest.clearAllMocks();
});

  it("should find available restaurants", async () => {
    (findRestaurants as jest.Mock).mockResolvedValueOnce(restaurants);

    const query = `
      query {
        findRestaurants(diners: ["Barney", "Spongebob"], time: "2023-10-07T10:00:00+00:00") {
          id
          name
        }
      }
    `;

    const response = await graphql({ source: query, schema });
    expect(response.data).toMatchObject({
      findRestaurants: [{ id: "1", name: "Don Camaron 🦐" }],
    });
    expect(findRestaurants).toHaveBeenCalledWith(
      ["Barney", "Spongebob"],
      "2023-10-07T10:00:00+00:00",
    );
  });

  it("should create a reservation and return the result", async () => {
    (findRestaurants as jest.Mock).mockResolvedValueOnce(restaurants);
    (createReservation as jest.Mock).mockResolvedValueOnce({
      id: 1,
      table: restaurants[0].tables[0],
      time: "2023-10-07T10:00:00.000Z",
    });

    const query = `
      query {
        findAndCreateReservation(diners: ["Barney", "Spongebob"], time: "2023-10-07T10:00:00.000Z") {
          availableRestaurants {
            id
            name
          }
          reservation {
            id
            table {
              id
            }
            time
          }
        }
      }
    `;

    const response = await graphql({ source: query, schema });
    expect(response.data).toMatchObject({
      findAndCreateReservation: {
        availableRestaurants: [{ id: "1", name: "Don Camaron 🦐" }],
        reservation: {
          id: "1",
          table: { id: "1" },
          time: "2023-10-07T10:00:00.000Z",
        },
      },
    });
    expect(createReservation).toHaveBeenCalledWith(1, [1, 2], "2023-10-07T10:00:00.000Z");
  });

  it("should delete a reservation", async () => {
    (deleteReservation as jest.Mock).mockResolvedValueOnce(true);

    const mutation = `
      mutation {
        deleteReservation(reservationId: 1)
      }
    `;

    const response = await graphql({ source: mutation, schema });
    expect(response.data).toMatchObject({
      deleteReservation: true,
    });
    expect(deleteReservation).toHaveBeenCalledWith(1);
  });
});
