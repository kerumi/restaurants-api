import { ObjectType, Field } from "type-graphql";
import Restaurant from "../../database/models/Restaurant";
import Reservation from "../../database/models/Reservation";

@ObjectType()
class ReservationResult {
  @Field(() => [Restaurant])
  availableRestaurants: Restaurant[];

  @Field(() => Reservation, { nullable: true })
  reservation?: Reservation;

  constructor(availableRestaurants: Restaurant[], reservation?: Reservation) {
    this.availableRestaurants = availableRestaurants;
    this.reservation = reservation;
  }
}

export default ReservationResult;
