import { Resolver, Query, Mutation, Arg } from "type-graphql";
import Restaurant from "../database/models/Restaurant";
import Reservation from "../database/models/Reservation";
import { createReservation, deleteReservation } from "../services/ReservationService";
import { findRestaurants } from "../services/RestaurantService";
import ReservationResult from "./types/ReservationResult";
import Eater from "../database/models/Eater";
import { dataSource } from "../config/database";

@Resolver(() => Restaurant)
class RestaurantResolver {
  private async getEaterIdsByNames(names: string[]): Promise<number[]> {
    const eaterRepo = dataSource.getRepository(Eater);
    const eaters = await eaterRepo.find({ where: names.map((name) => ({ name })) });

    if (eaters.length === 0) {
      throw new Error("The provided diners where not found");
    }

    return eaters.map((eater) => eater.id);
  }

  // Finds available restaurants
  @Query(() => [Restaurant], { description: "Find available restaurants" })
  async findRestaurants(
    @Arg("diners", () => [String]) diners: string[],
    @Arg("time", () => String) time: string,
  ): Promise<Restaurant[]> {
    return findRestaurants(diners, time);
  }

  // Finds available restaurants and creates a reservation
  @Query(() => ReservationResult, {
    description: "Find available restaurants and create a reservation",
  })
  async findAndCreateReservation(
    @Arg("diners", () => [String]) dinerNames: string[],
    @Arg("time", () => String) time: string,
  ): Promise<ReservationResult> {
    const dinerIds = await this.getEaterIdsByNames(dinerNames);
    const availableRestaurants = await findRestaurants(dinerNames, time);
    let reservation: Reservation | undefined;

    if (availableRestaurants.length > 0) {
      reservation = await createReservation(availableRestaurants[0].tables[0].id, dinerIds, time);
    }

    return new ReservationResult(availableRestaurants, reservation);
  }

  // Creates a reservation
  @Mutation(() => Reservation)
  async createReservation(
    @Arg("tableId", () => Number) tableId: number,
    @Arg("diners", () => [String]) dinerNames: string[],
    @Arg("time", () => String) time: string,
  ): Promise<Reservation> {
    const dinerIds = await this.getEaterIdsByNames(dinerNames);

    return createReservation(tableId, dinerIds, time);
  }

  // Remove a reservation
  @Mutation(() => Boolean)
  async deleteReservation(
    @Arg("reservationId", () => Number) reservationId: number,
  ): Promise<boolean> {
    return deleteReservation(reservationId);
  }
}

export default RestaurantResolver;
