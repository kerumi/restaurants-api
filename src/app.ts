import express, { Application } from "express";
import { graphqlHTTP } from "express-graphql";
import { buildSchema } from "type-graphql";
import path from "path";
import { globSync } from "glob";
import dotenv from "dotenv";
import connection from "./config/database";

dotenv.config();

const loadResolvers = async (pattern: string): Promise<Function[]> => {
  const files = globSync(pattern, { ignore: "**/*.test.ts" });
  const resolvers: Function[] = [];

  for (const file of files) {
    const module = await import(path.resolve(file));
    Object.values(module).forEach((resolver) => {
      if (typeof resolver === "function") {
        resolvers.push(resolver);
      }
    });
  }

  return resolvers;
};

const app: Application = express();

app.use(
  express.urlencoded({
    extended: true,
  }),
);
app.use(express.json({}));

const appConfig = async (): Promise<Application> => {
  const resolvers = await loadResolvers("src/resolvers/**/*.{ts,js}");

  if (resolvers.length === 0) {
    throw new Error("No resolvers found!");
  }

  await connection();

  const schema = await buildSchema({
    resolvers: resolvers as [Function, ...Function[]],
    nullableByDefault: true,
    emitSchemaFile: true,
  });

  app.use(
    "/graphql",
    graphqlHTTP({
      schema,
      graphiql: true,
    }),
  );

  return app;
};

export default appConfig;
