import "reflect-metadata";
import serverConfig from "./config/server";
import connectDatabase from "./config/database";
import app from "./app";

const PORT = serverConfig.port;

const bootstrap = async () => {
  try {
    await connectDatabase();
    console.log("Database connection established successfully");

    const serverApp = await app();
    const server = serverApp.listen(PORT, () => {
      console.log("\n");
      console.log(
        "App running in %s mode at http://localhost:%d",
        serverConfig.serverEnvironment,
        PORT,
      );
    });

    const gracefulShutdown = async (callType: string) => {
      console.error(`${callType} signal received.`);

      server.close(function (err: any) {
        if (err) {
          console.error("server error", err);
          process.exit(1);
        } else {
          process.exit(0);
        }
      });
    };

    process.on("SIGINT", async () => gracefulShutdown("SIGINT"));
  } catch (error) {
    console.error("Failed to initialize the server:", error);
    process.exit(1);
  }
};

bootstrap();

export default bootstrap;
