import { DataSource } from "typeorm";
import databaseConfig from "../../ormconfig";

const dataSource = new DataSource(databaseConfig);

const initializeDatabase = async () => {
  if (!dataSource.isInitialized) {
    await dataSource.initialize();
  }
  return dataSource;
};

export default initializeDatabase;
export { dataSource };
