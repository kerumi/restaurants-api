import { dataSource } from "../config/database";
import Restaurant from "../database/models/Restaurant";
import Table from "../database/models/Table";
import Eater from "../database/models/Eater";
import { In } from "typeorm";
import moment from "moment-timezone";

const findRestaurants = async (diners: string[], time: string): Promise<Restaurant[]> => {
  const timeDate = moment(time, moment.ISO_8601, true);
  if (!timeDate.isValid()) {
    throw new Error(`Invalid time format: ${time}`);
  }
  const utcTimeDate = timeDate.utc().toDate();

  const eaterRepo = dataSource.getRepository(Eater);
  const restaurantRepo = dataSource.getRepository(Restaurant);
  const tableRepo = dataSource.getRepository(Table);

  const eaters = await eaterRepo.find({
    where: {
      name: diners.length === 1 ? diners[0] : In(diners),
    },
  });

  if (eaters.length < diners.length) {
    throw new Error("Oops, one or more of the diners were not found.");
  }

  const dietaryRestrictions = new Set(eaters.flatMap((e) => e.dietaryRestrictions));
  const restaurants = await restaurantRepo.find({ relations: ["tables"] });

  const availableRestaurants = await Promise.all(
    restaurants.map(async (restaurant) => {
      const hasAllEndorsements = Array.from(dietaryRestrictions).every((restriction) =>
        restaurant.endorsements.includes(restriction),
      );

      if (!hasAllEndorsements) return null;

      const tablesAvailable = await Promise.all(
        restaurant.tables.map(async (table) => {
          if (table.capacity < eaters.length) return false;

          const reservationsCount = await tableRepo
            .createQueryBuilder("table")
            .leftJoinAndSelect("table.reservations", "reservations")
            .where("table.id = :tableId", { tableId: table.id })
            .andWhere("reservations.time = :time", { time: utcTimeDate })
            .getCount();

          return reservationsCount === 0;
        }),
      );

      if (tablesAvailable.some((isAvailable) => isAvailable)) {
        const filteredTables = restaurant.tables.filter((table) => table.capacity >= eaters.length);

        return {
          ...restaurant,
          tables: filteredTables,
        };
      }

      return null;
    }),
  );

  return availableRestaurants.filter((restaurant) => restaurant !== null) as Restaurant[];
};

export { findRestaurants };
