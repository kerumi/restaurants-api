import { dataSource } from "../config/database";
import Reservation from "../database/models/Reservation";
import Table from "../database/models/Table";
import Eater from "../database/models/Eater";
import moment from "moment-timezone";

const createReservation = async (
  tableId: number,
  dinerIds: number[],
  time: string,
): Promise<Reservation> => {
  const timeDate = moment(time, moment.ISO_8601, true);

  if (!timeDate.isValid()) {
    throw new Error(`Invalid time format: ${time}`);
  }

  const utcTimeDate = timeDate.utc().toDate();
  const reservationEnd = moment(utcTimeDate).add(2, "hours").toDate();

  const tableRepo = dataSource.getRepository(Table);
  const reservationRepo = dataSource.getRepository(Reservation);
  const eaterRepo = dataSource.getRepository(Eater);

  const table = await tableRepo.findOne({
    where: { id: tableId },
    relations: ["reservations", "restaurant"],
  });

  if (!table) {
    throw new Error("Table not found");
  }

  const reservationsOverlap = await reservationRepo
    .createQueryBuilder("reservation")
    .where("reservation.tableId = :tableId", { tableId })
    .andWhere("reservation.time = :time", { time: utcTimeDate })
    .getCount();

  if (reservationsOverlap > 0) {
    throw new Error("Table is already reserved at this time");
  }

  const overlappingReservation = await reservationRepo
    .createQueryBuilder("reservation")
    .leftJoin("reservation.diners", "diner")
    .where("diner.id IN (:...dinerIds)", { dinerIds })
    .andWhere(`(reservation.time, reservation.time + INTERVAL '2 hours') OVERLAPS (:start, :end)`, {
      start: utcTimeDate,
      end: reservationEnd,
    })
    .getCount();

  if (overlappingReservation > 0) {
    throw new Error("One or more diners have overlapping reservations in the given timeframe");
  }

  const dinersList = await eaterRepo.findByIds(dinerIds);

  const reservation = reservationRepo.create({
    table,
    diners: dinersList,
    time: utcTimeDate,
  });

  await reservationRepo.save(reservation);

  return reservation;
};

const deleteReservation = async (reservationId: number): Promise<boolean> => {
  const reservationRepo = dataSource.getRepository(Reservation);

  const reservation = await reservationRepo.findOne({ where: { id: reservationId } });

  if (!reservation) {
    throw new Error("Reservation not found");
  }

  await reservationRepo.remove(reservation);

  return true;
};

export { createReservation, deleteReservation };
