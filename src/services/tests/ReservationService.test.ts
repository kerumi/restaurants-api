import { mock, MockProxy } from "jest-mock-extended";
import { Repository } from "typeorm";
import { DataSource } from "typeorm/data-source/DataSource";
import Reservation from "../../database/models/Reservation";
import Table from "../../database/models/Table";
import Eater from "../../database/models/Eater";
import * as ReservationService from "../../services/ReservationService";
import moment from "moment-timezone";

jest.mock("moment-timezone", () => {
  const originalMoment = jest.requireActual("moment-timezone");
  const momentMock = jest.fn((...args) => {
    if (args.length) {
      return originalMoment(...args);
    }
    return originalMoment();
  });

  Object.assign(momentMock, originalMoment);
  return momentMock;
});

describe("ReservationService", () => {
  let mockDataSource: MockProxy<DataSource>;
  let mockReservationRepo: MockProxy<Repository<Reservation>>;
  let mockTableRepo: MockProxy<Repository<Table>>;
  let mockEaterRepo: MockProxy<Repository<Eater>>;

  beforeAll(() => {
    mockDataSource = mock<DataSource>();
    mockReservationRepo = mock<Repository<Reservation>>();
    mockTableRepo = mock<Repository<Table>>();
    mockEaterRepo = mock<Repository<Eater>>();

    mockDataSource.getRepository.mockImplementation((entity) => {
      switch (entity) {
        case Reservation:
          return mockReservationRepo;
        case Table:
          return mockTableRepo;
        case Eater:
          return mockEaterRepo;
        default:
          throw new Error("Unknown entity");
      }
    });

    require("../../config/database").dataSource = mockDataSource;
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should create a reservation successfully", async () => {
    const tableId = 1;
    const dinerIds = [1, 2];
    const time = "2023-10-07T10:00:00+00:00";
    const timeDate = moment(time, moment.ISO_8601, true);

    (moment as unknown as jest.Mock).mockReturnValueOnce(timeDate);

    const table = new Table();
    table.id = tableId;
    table.reservations = [];

    mockTableRepo.findOne.mockResolvedValueOnce(table);
    mockReservationRepo.createQueryBuilder.mockReturnValue({
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      getCount: jest.fn().mockResolvedValueOnce(0).mockResolvedValueOnce(0),
      leftJoin: jest.fn().mockReturnThis(),
    } as any);
    mockEaterRepo.findByIds.mockResolvedValueOnce([
      { id: 1, name: "Alice", dietaryRestrictions: [] } as Eater,
      { id: 2, name: "Bob", dietaryRestrictions: [] } as Eater,
    ]);

    const createdReservation = new Reservation();
    mockReservationRepo.create.mockReturnValueOnce(createdReservation);
    mockReservationRepo.save.mockResolvedValueOnce(createdReservation);

    const result = await ReservationService.createReservation(tableId, dinerIds, time);

    expect(result).toBe(createdReservation);
  });

  it("should throw an error if reservation time is invalid", async () => {
    const tableId = 1;
    const dinerIds = [1, 2];
    const time = "invalid-time-format";

    (moment as unknown as jest.Mock).mockReturnValueOnce({
      isValid: jest.fn().mockReturnValue(false),
    } as any);

    await expect(ReservationService.createReservation(tableId, dinerIds, time)).rejects.toThrow(
      `Invalid time format: ${time}`,
    );
  });

  it("should delete a reservation successfully", async () => {
    const reservationId = 1;
    const reservation = new Reservation();
    reservation.id = reservationId;

    mockReservationRepo.findOne.mockResolvedValueOnce(reservation);
    mockReservationRepo.remove.mockResolvedValueOnce(reservation);

    const result = await ReservationService.deleteReservation(reservationId);

    expect(result).toBe(true);
  });

  it("should throw an error if reservation is not found for deletion", async () => {
    const reservationId = 1;
    mockReservationRepo.findOne.mockResolvedValueOnce(undefined);

    await expect(ReservationService.deleteReservation(reservationId)).rejects.toThrow(
      "Reservation not found",
    );
  });
});
