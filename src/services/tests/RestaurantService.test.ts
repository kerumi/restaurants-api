import { mock, MockProxy } from "jest-mock-extended";
import { Repository } from "typeorm";
import { DataSource } from "typeorm/data-source/DataSource";
import Restaurant from "../../database/models/Restaurant";
import Table from "../../database/models/Table";
import Eater from "../../database/models/Eater";
import { findRestaurants } from "../RestaurantService";
import moment from "moment-timezone";

jest.mock("moment-timezone", () => {
  const originalMoment = jest.requireActual("moment-timezone");
  const momentMock = jest.fn((...args) => {
    if (args.length) {
      return originalMoment(...args);
    }
    return originalMoment();
  });

  Object.assign(momentMock, originalMoment);
  return momentMock;
});

describe("RestaurantService", () => {
  let mockDataSource: MockProxy<DataSource>;
  let mockRestaurantRepo: MockProxy<Repository<Restaurant>>;
  let mockTableRepo: MockProxy<Repository<Table>>;
  let mockEaterRepo: MockProxy<Repository<Eater>>;

  beforeAll(() => {
    mockDataSource = mock<DataSource>();
    mockRestaurantRepo = mock<Repository<Restaurant>>();
    mockTableRepo = mock<Repository<Table>>();
    mockEaterRepo = mock<Repository<Eater>>();

    mockDataSource.getRepository.mockImplementation((entity) => {
      switch (entity) {
        case Restaurant:
          return mockRestaurantRepo;
        case Table:
          return mockTableRepo;
        case Eater:
          return mockEaterRepo;
        default:
          throw new Error("Unknown entity");
      }
    });

    require("../../config/database").dataSource = mockDataSource;
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should find restaurants successfully", async () => {
    const diners = ["Carlos", "Mad Max"];
    const time = "2023-10-07T10:00:00+00:00";
    const timeDate = moment(time, moment.ISO_8601, true);

    (moment as unknown as jest.Mock).mockReturnValueOnce(timeDate);

    const eater1 = { id: 1, name: "Carlos", dietaryRestrictions: ["Vegan"] } as Eater;
    const eater2 = { id: 2, name: "Mad Max", dietaryRestrictions: ["Gluten-Free"] } as Eater;

    const table1 = { id: 1, capacity: 4, reservations: [] } as Table;
    const table2 = { id: 2, capacity: 4, reservations: [] } as Table;

    const restaurant = {
      id: 1,
      name: "Fancy Restaurant",
      tables: [table1, table2],
      endorsements: ["Vegan", "Gluten-Free"],
    } as Restaurant;

    mockEaterRepo.find.mockResolvedValueOnce([eater1, eater2]);
    mockRestaurantRepo.find.mockResolvedValueOnce([restaurant]);
    mockTableRepo.createQueryBuilder.mockReturnValue({
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      getCount: jest.fn().mockResolvedValueOnce(0),
    } as any);

    const result = await findRestaurants(diners, time);

    expect(result).toHaveLength(1);
    expect(result[0].name).toBe("Fancy Restaurant");
    expect(result[0].tables).toHaveLength(2);
  });

  it("should throw an error if time format is invalid", async () => {
    const diners = ["Carlos", "Mad Max"];
    const time = "invalid-time-format";

    (moment as unknown as jest.Mock).mockReturnValueOnce({
      isValid: jest.fn().mockReturnValue(false),
    } as any);

    await expect(findRestaurants(diners, time)).rejects.toThrow(`Invalid time format: ${time}`);
  });

  it("should throw an error if some diners are not found", async () => {
    const diners = ["Carlos", "Mad Max"];
    const time = "2023-10-07T10:00:00+00:00";
    const timeDate = moment(time, moment.ISO_8601, true);

    (moment as unknown as jest.Mock).mockReturnValueOnce(timeDate);

    mockEaterRepo.find.mockResolvedValueOnce([
      { id: 1, name: "Carlos", dietaryRestrictions: [] } as Eater,
    ]);

    await expect(findRestaurants(diners, time)).rejects.toThrow(
      "Oops, one or more of the diners were not found.",
    );
  });

  it("should not return restaurants without valid endorsements", async () => {
    const diners = ["Carlos", "Mad Max"];
    const time = "2023-10-07T10:00:00+00:00";
    const timeDate = moment(time, moment.ISO_8601, true);

    (moment as unknown as jest.Mock).mockReturnValueOnce(timeDate);

    const eater1 = { id: 1, name: "Carlos", dietaryRestrictions: ["Vegan"] } as Eater;
    const eater2 = { id: 2, name: "Mad Max", dietaryRestrictions: ["Gluten-Free"] } as Eater;

    const table1 = { id: 1, capacity: 4, reservations: [] } as Table;
    const table2 = { id: 2, capacity: 4, reservations: [] } as Table;

    const restaurantWithoutEndorsements = {
      id: 1,
      name: "Ordinary Restaurant",
      tables: [table1, table2],
      endorsements: ["Non-Vegetarian"],
    } as Restaurant;

    mockEaterRepo.find.mockResolvedValueOnce([eater1, eater2]);
    mockRestaurantRepo.find.mockResolvedValueOnce([restaurantWithoutEndorsements]);

    const result = await findRestaurants(diners, time);

    expect(result).toHaveLength(0);
  });

  it("should not return restaurants without available tables", async () => {
    const diners = ["Carlos", "Mad Max"];
    const time = "2023-10-07T10:00:00+00:00";
    const timeDate = moment(time, moment.ISO_8601, true);

    (moment as unknown as jest.Mock).mockReturnValueOnce(timeDate);

    const eater1 = { id: 1, name: "Carlos", dietaryRestrictions: ["Vegan"] } as Eater;
    const eater2 = { id: 2, name: "Mad Max", dietaryRestrictions: ["Gluten-Free"] } as Eater;

    const table1 = { id: 1, capacity: 4, reservations: [] } as Table;
    const table2 = { id: 2, capacity: 4, reservations: [] } as Table;

    const restaurantWithTables = {
      id: 1,
      name: "Fancy Restaurant",
      tables: [table1, table2],
      endorsements: ["Vegan", "Gluten-Free"],
    } as Restaurant;

    mockEaterRepo.find.mockResolvedValueOnce([eater1, eater2]);
    mockRestaurantRepo.find.mockResolvedValueOnce([restaurantWithTables]);
    mockTableRepo.createQueryBuilder.mockReturnValue({
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      getCount: jest.fn().mockResolvedValue(1),
    } as any);

    const result = await findRestaurants(diners, time);

    expect(result).toHaveLength(0);
  });
});
