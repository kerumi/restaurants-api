# Restaurants API

## Restaurant Booking API with a Social Twist

This project is a Restaurant Booking API that allows users to find available restaurants, create, and manage reservations with a social twist. Below are the steps to set up and run the project, as well as examples on how to test the GraphQL API.

## Testing the Demo

You can test the API [demo here](http://api.restaurants.puchi.net/graphql)

This demo contains some sample Diners, so you can test the API without having to create them manually.

### Diners

 id |         name         |     dietaryRestrictions
----+----------------------+------------------------------
  1 | Ruben Effertz        | vegetarian,halal
  2 | Tracey Schumm        |
  3 | Ernest Turner        | vegetarian,halal
  4 | Everett Leffler DVM  | halal
  5 | Freda Aufderhar II   | halal,vegetarian,gluten-free
  6 | Marty Rau-Hilll      | vegetarian,halal,vegan
  7 | Dr. Ellis Schowalter |
  8 | Allison Adams        |
  9 | Shelia Abshire       |
 10 | Christian Beatty DDS | vegan

### Restaurants

 id |               name                |            endorsements
----+-----------------------------------+------------------------------------
  1 | Pagac, Kovacek and Schaefer       | vegan
  2 | Lebsack - Bartoletti              | gluten-free,halal,vegetarian
  3 | Christiansen, Weimann and Muller  | vegetarian,halal,vegan,gluten-free
  4 | Oberbrunner, Padberg and Reynolds | vegan,gluten-free,halal,vegetarian
  5 | Jacobi LLC                        | vegetarian,vegan

## Setup

### Prerequisites

- Node.js (v18+)
- TypeScript
- PostgreSQL

### Install Dependencies

```bash
cp .env.example .env
```

Example .env file

```bash
DB_HOST=<your-host>
DB_USER=postgres
DB_PASSWORD=password
DB_NAME=db
DB_PORT=5432
```

#### Run database migrations

```bash
pnpm dev:migrate:generate ./src/database/migrations/<name>
```

#### Run database seeds

```bash
pnpm dev:seed:run
```

### Run the Application

```bash
pnpm dev
```

### Run the unit tests

```bash
pnpm test
```

### Access GraphQL Playground

Open your browser and navigate to http://localhost:9200/graphql to access the GraphQL Playground.

## GraphQL API Examples

### Find Available Restaurants

```graphql
{
  findRestaurants(
    diners: ["Antonio Stokes", "Christy Kunde", "Lola Harber"]
    time: "2024-10-05T19:00:00-04:00"
  ) {
    id
    name
    endorsements
    tables {
      id
      capacity
    }
  }
}
```

### Find Available Restaurants and Create a Reservation

```graphql
{
  findAndCreateReservation(
    diners: ["Antonio Stokes", "Christy Kunde", "Lola Harber"]
    time: "2024-10-05T24:00:00-04:00"
  ) {
    availableRestaurants {
      id
      name
      endorsements
    }
    reservation {
      id
      time
      table {
        id
        restaurant {
          name
        }
        capacity
      }
    }
  }
}
```

### Create a Reservation

```graphql
mutation {
  createReservation(tableId: 1, diners: ["Antonio Stokes"], time: "2023-10-10T18:00:00Z") {
    id
    table {
      id
    }
    diners {
      id
      name
    }
    time
  }
}
```

### Delete a Reservation

```graphql
mutation {
  deleteReservation(reservationId: 1)
}
```

## Docker Setup

If you're running docker in your machine, you can use it to run the project.

### Run and build the application

```bash
docker-compose up -d --build
```

This will build the project and run it in the background, also creating the database, running migrations and seeds.

### Stopping the application

```bash
docker-compose down
```
