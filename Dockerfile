# Ops ops by kerumi

# Using the official Node.js 18 image as the base image
FROM node:18

# Set the working directory inside the container
WORKDIR /app

# Copy the package.json, pnpm-lock.yaml, and tsconfig.json to the container
COPY package.json pnpm-lock.yaml tsconfig.json ./

# Install pnpm globally
RUN npm install -g pnpm

# Install dependencies using pnpm
RUN pnpm install

# Copy the rest of the application code to the container
COPY . .

# Copy the wait-for-it.sh script to the container
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /usr/local/bin/wait-for-it.sh

# Make sure it's executable
RUN chmod +x /usr/local/bin/wait-for-it.sh

# Set environment variables for build
ENV DB_HOST=db
ENV DB_USER=postgres
ENV DB_PASSWORD=password
ENV DB_NAME=restaurants
ENV DB_PORT=5432

# Build the TypeScript project
RUN pnpm run build:ts

# Expose the port your app runs on
EXPOSE 9200

# Command to run the application with wait-for-it script
CMD ["wait-for-it.sh", "db:5432", "--", "sh", "-c", "pnpm run build:migrate && pnpm run build:seed && pnpm start"]
