import "dotenv/config";
import { DataSourceOptions } from "typeorm";

const { DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME } = process.env;

const databaseConfig: DataSourceOptions = {
  type: "postgres",
  host: DB_HOST,
  port: parseInt(DB_PORT || "5432", 10),
  username: DB_USER,
  password: DB_PASSWORD,
  database: DB_NAME,
  logging: true,
  maxQueryExecutionTime: 10_000,
  connectTimeoutMS: 10_000,
  synchronize: false,
  entities: [`${__dirname}/src/database/models/**/*{.ts,.js}`],
  migrations: [`${__dirname}/src/database/migrations/**/*{.ts,.js}`],
};

export default {
  ...databaseConfig,
  seeds: [`${__dirname}/src/database/seeds/**/*{.ts,.js}`],
  factories: [`${__dirname}/src/database/seedFactories/**/*{.ts,.js}`],
  cli: {
    entitiesDir: "src/database/models",
    migrationsDir: "src/database/migrations",
  },
};
